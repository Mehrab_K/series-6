package sbu.cs.exception;

public class UnrecognizedCommandException extends ApException
{

    public UnrecognizedCommandException(String message)
    {
        super(message);
    }
}
