package sbu.cs.multithread.semaphor;

public class Chef extends Thread
{
    private String name;

    public Chef(String name)
    {
        this.name = name;
    }

    @Override
    public void run()
    {
        for (int i = 0; i < 10; i++)
        {
            try
            {
                Source.getSource(name);
                sleep(1000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }
}
