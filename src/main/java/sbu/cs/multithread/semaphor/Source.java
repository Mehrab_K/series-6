package sbu.cs.multithread.semaphor;

import java.util.concurrent.Semaphore;

public class Source
{
    private static Semaphore semaphore = new Semaphore(2);

    public static void getSource(String threadName) throws InterruptedException
    {
        semaphore.acquire();

        try
        {
            System.out.println(threadName + " is in the source");
            Thread.sleep(10);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        semaphore.release();
    }
}
