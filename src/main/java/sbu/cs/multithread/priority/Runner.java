package sbu.cs.multithread.priority;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

public class Runner
{
    public static CountDownLatch blackCounter, whiteCounter, blueCounter;
    public static List<Message> messages = new ArrayList<>();

    /**
     * add your codes to this function. this function is the caller function which will be called first.
     * changing other codes in this function is allowed.
     *
     * @param blackCount    number of black threads
     * @param blueCount     number of blue threads
     * @param whiteCount    number of white threads
     */
    public void run(int blackCount, int blueCount, int whiteCount)
    {
        List<ColorThread> colorThreads = new ArrayList<>();
        ExecutorService service = Executors.newFixedThreadPool(10);


        blackCounter = new CountDownLatch(blackCount);
        IntStream.range(0, blackCount).forEach(value ->
        {
            BlackThread blackThread = new BlackThread();
            colorThreads.add(blackThread);
            service.submit(blackThread);
        });

        try
        {
            blackCounter.await();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        blueCounter = new CountDownLatch(blueCount);
        IntStream.range(0, blueCount).forEach(value ->
        {
            BlueThread blueThread = new BlueThread();
            colorThreads.add(blueThread);
            service.submit(blueThread);
        });

        try
        {
            blueCounter.await();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }


        whiteCounter = new CountDownLatch(whiteCount);
        IntStream.range(0, whiteCount).forEach(value ->
        {
            WhiteThread whiteThread = new WhiteThread();
            colorThreads.add(whiteThread);
            service.submit(whiteThread);
        });

        try
        {
            whiteCounter.await();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        service.shutdown();
    }


    synchronized public static void addToList(Message message)
    {
        messages.add(message);
    }

    public List<Message> getMessages()
    {
        return messages;
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args)
    {
    }
}
