package sbu.cs.multithread.pi;


public class PICalculator
{
    public String calculate(int floatingPoint)
    {
        PI_Calculator pi_calculator = new PI_Calculator(floatingPoint + 2);

        String PI = pi_calculator.compute().toString();

        return new StringBuilder(PI).deleteCharAt(PI.length() - 1).toString();
    }
}
