package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class PI_Calculator
{
    private int numDigits;
    private MathContext mathContext;

    public PI_Calculator(int numDigits)
    {
        this.numDigits = numDigits;
        mathContext = new MathContext(numDigits);
        pi  = new BigDecimal(0);
    }

    /**
     * variables which changes in threads :
     * pi  ->  PI number
     * pi_n  ->  dividing PI to a Sequence of (n)
     */
    volatile private BigDecimal pi;
    volatile private BigDecimal pi_n;

    /**
     * compute function which is work multi thread
     * @return PI
     */

    public BigDecimal compute()
    {
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch counter = new CountDownLatch(numDigits);

        for (int k = 0; k <= numDigits; k++)
        {
            int finalI = k;

            Thread thread = new Thread(() ->
            {
                pi_n = piFunction(finalI);
                add(pi_n);
                counter.countDown();
            });

            service.submit(thread);
        }

        service.shutdown();

        try
        {
            counter.await();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        return pi.round(mathContext);
    }


    synchronized private void add(BigDecimal num)
    {
        pi = pi.add(num);
    }


    /**
     * this func returns the value of pi_n sequence
     * @param k which is a index of sequence
     * @return value of index k
     */
    synchronized private BigDecimal piFunction(int k)
    {
        int k8 = 8 * k;

        BigDecimal val1 = new BigDecimal(4);
        val1 = val1.divide(new BigDecimal(k8 + 1), mathContext);
        BigDecimal val2 = new BigDecimal(-2);
        val2 = val2.divide(new BigDecimal(k8 + 4), mathContext);
        BigDecimal val3 = new BigDecimal(-1);
        val3 = val3.divide(new BigDecimal(k8 + 5), mathContext);
        BigDecimal val4 = new BigDecimal(-1);
        val4 = val4.divide(new BigDecimal(k8 + 6), mathContext);

        BigDecimal val = val1;

        val = val.add(val2);
        val = val.add(val3);
        val = val.add(val4);

        BigDecimal multiplier = new BigDecimal(16);
        multiplier = multiplier.pow(k);

        BigDecimal one = new BigDecimal(1);
        multiplier = one.divide(multiplier, mathContext);
        val = val.multiply(multiplier);

        return val;
    }

}
